﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ConsoleApp;
namespace UnitTest
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {

            //Arrange - Created and object of Math Class
            MathClass obj = new MathClass();

            //Act - What we are expecting 
            int result = obj.Add(10, 20);

            //Assert - comparing
            Assert.AreEqual(30, result);
        }

    }
}
